<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class pembelian_h_model extends CI_Model
{
	//panggil nama table
	private $_table_header = "pembelian_h";
	private $_table_detail = "pembelian_d";
	
	public function rules()
	{
		return
		[
			[
				'field' => 'no_trans',
				'label'	=> 'no trans',
				'rules' => 'required|max_length[10]',
				'errors' =>[
					'required' => 'no transaksi tidak Boleh Kosong',
					'max_length' => 'no transaksi tdak Boleh Lebih dari 10 karakter',
				],
			],
			[
				'field' => 'kode_supplier',
				'label'	=> 'kode supplier',
				'rules' => 'required',
				'errors' =>[
					'required' => 'nama tidak Boleh Kosong',
				],
			]
		];
	}
	public function rules1()
	{
		return
		[
			[
				'field' => 'kode_barang',
				'label'	=> 'kode barang',
				'rules' => 'required|max_length[5]',
				'errors' =>[
					'required' => 'kode barang tidak Boleh Kosong',
					'max_length' => 'kode barang tdak Boleh Lebih dari 10 karakter',
				],
			],
			[
				'field' => 'qty',
				'label'	=> 'qty',
				'rules' => 'required',
				'errors' =>[
					'required' => 'qty tidak Boleh Kosong',
				],
			],
			[
				'field' => 'harga',
				'label'	=> 'harga',
				'rules' => 'required',
				'errors' =>[
					'required' => 'harga tidak Boleh Kosong',
				],
			]
		];
	}
	
	public function tampilDataPembelian_h()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table_header)->result();
	}

	public function tampilDatapembelian_h2()
	{
		//menggunakan query
		$query = $this->db->query("SELECT * FROM" . $this->_table_header . "where flag = 1");
		return $query->result();
	}

	public function tampilDataPembelian_h3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('id_pembelian_h', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$data['no_trans']			= $this->input->post('no_trans');
		$data['kode_supplier']		= $this->input->post('kode_supplier');
		$data['tanggal']			= date ('y-m-d');
		$data['approved']			= 1;
		$data['flag']				= 1;
		$this->db->insert($this->_table_header, $data);


	}
	public function idTransaksiTerakhir()
	{
		$query	= $this->db->query("SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_pembelian_h DESC LIMIT 0,1 ");
		$data_id = $query->result();

		foreach ($data_id as $data ) {
			# code...
			$last_id = $data->id_pembelian_h;
		}
		return $last_id;
	}
	public function tampilDataPembelianDetail($id)
	{
		  $query	= $this->db->query(
            "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_pembelian_h = '$id'"
        );
        return $query->result();


	}
	 public function savePembelianDetail($id)
    {
        $qty    = $this->input->post('qty');
        $harga  = $this->input->post('harga');

        $data['id_pembelian_h'] = $id;
        $data['kode_barang']    = $this->input->post('kode_barang');
        $data['qty']            = $qty;
        $data['harga']          = $harga;
        $data['jumlah']         = $qty * $harga;
        $data['flag']           = 1;

        $this->db->insert($this->_table_detail, $data);
    }


}