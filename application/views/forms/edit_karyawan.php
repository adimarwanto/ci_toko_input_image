<table width="1094" border="0">
  <tr align="center" >
    <td width="1084">
      <?= validation_errors();?>
      <?php
foreach ($detail_karyawan as $data){
  $nik  =$data->nik;
  $nama_lengkap =$data->nama_lengkap;
  $tempat_lahir =$data->tempat_lahir;
  $tgl_lahir  =$data->tgl_lahir;
  $jenis_kelamin  =$data->jenis_kelamin;
  $alamat =$data->alamat;
  $telp =$data->telp;
  $kode_jabatan =$data->kode_jabatan;
  $photo  = $data->photo;
}
//pisal tanggal
  $thn_pisah = substr($tgl_lahir, 0, 4);
  $bln_pisah = substr($tgl_lahir, 5, 2);
  $tgl_pisah = substr($tgl_lahir, 8, 2);
?>
      <form action="<?=base_url();?>karyawan/Edit/<?=$nik;?>" method="post" name="form1" id="form1"
        enctype="multipart/form-data">
        <div align="center">
          <h1>Edit Karyawan</h1>
        </div>
        <table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00FF00">
          <tr>
            <td width="42%">NIK</td>
            <td width="3%"> :</td>
            <td width="55%"><input type="text" readonly="readonly" name="nik" id="nik" maxlength="50" value="<?= $nik;?>" /></td>
          </tr>
          <tr>
            <td>Nama</td>
            <td>:</td>
            <td><input type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50" value="<?= $nama_lengkap;?>" /></td>
          </tr>
          <tr>
            <td>Tempat Lahir</td>
            <td>:</td>
            <td><input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50" value="<?= $tempat_lahir;?>" /></td>
          </tr>
          <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td><select name="jenis_kelamin" id="jenis_kelamin" >
              <?php
          $jk = ['L','P'];
          $jk_text= ['L' => 'Laki-Laki', 'P' => 'Perempuan'];

          foreach ($jk as $data) {
            # code...
            if ($data == $jenis_kelamin) {
              # code...
              $select = 'selected';
            }else {
              $select = '';
            }
          
        ?>
              <option value="<?= $data;?>" <?=$select;?>>
                <?=$jk_text[$data];?>
                </option>
              <?php } ?>
            </select></td>
          </tr>
          <tr>
            <td>Tanggal Lahir</td>
            <td>:</td>
            <td><select name="tgl" id="tgl" >
              <?php
      	for($tgl=1;$tgl<=31;$tgl++){
          // RUMUS IF BARU 
          $select_tgl= ($tgl == $tgl_pisah) ? 'selected' : '';
      ?>
              <option value="<?=$tgl;?>" <?=$select_tgl;?>>
                <?=$tgl;?>
                </option>
              <?php
       	}
       ?>
            </select>
              <select name="bln" id="bln">
                <?php
      	$bulan_n = array('Januari','Februari','Maret','April',
        				'Mei','Juni','Juli','Agustus','September',
                        'Oktober','November','Desember');
         for($bln=0;$bln<12;$bln++){
          $select_bln= ($bln == $bln_pisah-1) ? 'selected' : '';
      ?>
                <option value="<?=$bln+1;?>" <?=$select_bln;?>>
                  <?=$bulan_n[$bln];?>
                </option>
                <?php
        	}
        ?>
              </select>
              <select name="thn" id="thn">
                <?php
      	for($thn = date('Y')-60;$thn<=date('Y');$thn++){
          $select_thn= ($thn == $thn_pisah) ? 'selected' : '';
      ?>
                <option value="<?=$thn;?>" <?=$select_thn;?>>
                  <?=$thn;?>
                </option>
                <?php
      	} 
      ?>
              </select></td>
          </tr>
          <tr>
            <td>Telepon</td>
            <td>:</td>
            <td><input type="text" name="telp" id="telp" maxlength="50" value="<?= $telp;?>" /></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><textarea  name="alamat" id="alamat" cols="45" rows="5" ><?= $alamat;?>
  </textarea></td>
          </tr>
          <tr>
            <td>Jabatan</td>
            <td>:</td>
            <td><select name="kode_jabatan" id="kode_jabatan" >
              <?php foreach ($data_jabatan as $data){ 
          $select_jabatan = ($data->kode_jabatan == $kode_jabatan) ? 'selected' : '';
          ?>
              <option value="<?=$data->kode_jabatan;?>" <?=$select_jabatan;?>>
                <?=$data->nama_jabatan; ?>
                </option>
              <?php }?>
            </select></td>
          </tr>
          <tr>
              <td>Upload Foto</td>
              <td>:</td>
              <td>
                <input type="file" name="image" id="image" >
                <input type="hidden" name="foto_old" id="foto_old" value="<?= $photo;?>">

              </td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="Submit" id="Submit" value="Simpan" />
              <input type="reset" name="reset" id="reset" value="Reset" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><a href="<?=base_url();?>karyawan/listkaryawan">
              <input type="button" name="kembali" id="kembali" value="Kembali ke Menu Sebelumnya" />
            </a></td>
          </tr>
        </table>
      </form>
      <?php  ?></td>
  </tr>
</table>
