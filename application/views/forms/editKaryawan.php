<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Master</title>
</head>
<body>
<table width="100%" border="0">
  <tr align="center">
    <td height="80"><h2>Edit Karyawan</h2></td>
  </tr>
  <tr align="center" bgcolor="#CCCCCC">
    <td><?php


	foreach($editKaryawan as $data){
		$nik		=$data->nik;
		$nama_lengkap=$data->nama_lengkap;
		$tempat_lahir=$data->tempat_lahir;
		$jenis_kelamin=$data->jenis_kelamin;
		$alamat=$data->alamat;
		$telp=$data->telp;
		$kode_jabatan=$data->kode_jabatan;
		$tgl_lahir=$data->tgl_lahir;
		}
		$thn_pisah=substr($tgl_lahir,0,4);
		$bln_pisah=substr($tgl_lahir,5,2);
		$tgl_pisah=substr($tgl_lahir,8,2);

?>
  <tr>
      <form name="form" method="POST" action="<?=base_url();?>karyawan/editKaryawan/<?=$nik;?>">
        <td><table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="">
          <tr>
            <td width="37%">NIK</td>
            <td width="4%"> :</td>
            <td width="59%"><input type="text" name="nik" id="nik" maxlength="50" value="<?=$nik?>"></td>
          </tr>
          <tr>
            <td>Nama</td>
            <td>:</td>
            <td><input type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50" value="<?=$nama_lengkap;?>"></td>
          </tr>
          <tr>
            <td>Tempat Lahir</td>
            <td>:</td>
            <td><input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50" value="<?=$tempat_lahir;?>"></td>
          </tr>
          <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td><select name="jenis_kelamin" id="jenis_kelamin">
              <?php
            $jk=['L','P'];
            $jk_text=['L'=>'Laki-Laki','P'=>'Perempuan'];
            foreach ($jk as $data) {
            	if($data==$jenis_kelamin){
             	$select="selected";
             } else{
             	$select='';
             }

            ?>
              <option value="<?=$data;?>" <?=$select;?>>
                <?=$jk_text[$data];?>
                </option>
              <?php } ?>
            </select></td>
          </tr>
          <tr>
            <td>Tanggal Lahir</td>
            <td>:</td>
            <td><select name="tgl" id="tgl">
              <?php
      	for($tgl=1;$tgl<=31;$tgl++){
      		//rumus if baru
      		$select_tgl=($tgl==$tgl_pisah)?'selected':'';
      ?>
              <option value="<?=$tgl;?>" <?=$select_tgl;?>>
                <?=$tgl;?>
                </option>
              <?php
       	}
       ?>
            </select>
              <select name="bln" id="bln">
                <?php
      	$bulan_n = array('Januari','Februari','Maret','April',
        				'Mei','Juni','Juli','Agustus','September',
                        'Oktober','November','Desember');
         for($bln=1;$bln<12;$bln++){
         	$select_bln=($bln==$bln_pisah-1)?'selected':'';
      ?>
                <option value="<?=$bln+1;?>" <?=$select_bln;?>>
                  <?=$bulan_n[$bln];?>
                </option>
                <?php
        	}
        ?>
              </select>
              <select name="thn" id="thn">
                <?php
      	for($thn = date('Y')-60;$thn<=date('Y');$thn++){
      		$select_thn=($thn==$thn_pisah)?'selected':'';
      ?>
                <option value="<?=$thn;?>" <?=$select_thn;?>>
                  <?=$thn;?>
                </option>
                <?php
      	} 
      ?>
              </select></td>
          </tr>
          <tr>
            <td>Telepon</td>
            <td>:</td>
            <td><input type="text" name="telp" id="telp" maxlength="15" value="<?=$telp;?>"></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><textarea name="alamat" id="alamat" cols="45" rows="5"><?=$alamat;?>
</textarea></td>
          </tr>
          <tr>
            <td>Jabatan</td>
            <td>:</td>
            <td><select name="nama_jabatan" id="nama_jabatan">
              <?php foreach($data_jabatan as $data) {
          	$select_jabatan=($data->kode_jabatan==$kode_jabatan)?'selected':'';
           ?>
              <option value="<?=$data->kode_jabatan;?>" <?=$select_jabatan;?>>
                <?=$data->nama_jabatan;?>
                </option>
              <?php } ?>
            </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="Submit" id="Submit" value="Simpan">
              <input type="reset" name="reset" id="reset" value="Reset"></td>
          </tr>
          <tr>
            <td align="center"><a href="<?=base_url();?>karyawan">
              <input type="button" name="Kembali2" id="Kembali2" value="Kembali Ke Menu Sebelumnya">
            </a></td>
          </tr>
        </table>
    </form>
</table>
</body>
</html>