<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class auth extends CI_Controller
{

	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("user_model");
	}	
	public function index()
	{
		//cek login akses
		$user_login = $this->session->userdata();
		if (count($user_login)<= 1) {
			# code...
			$this->login();
		}else{
			redirect("home/");
		}
	}
	public function login()
	{
		if (!empty($_REQUEST)) {
			# code...
			//ambil dari form login
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$data_login = $this->user_model->cekUser($username, $password);

			//echo "<pre>";
			//print_r($data_login); die();
			//echo "</pre>";

			$data_sesi=[
				'username' 	=> $data_login['nik'],
				'email' 	=> $data_login['email'],
				'tipe' 		=> $data_login['tipe'],
				'status' 	=> "login"
			];

			if (!empty($data_login)) {
				# code...
				//login berhasil
				$this->session->set_userdata($data_sesi);
				redirect("karyawan/", "refresh");
				}else{
					//login gagal
					$this->session->set_flashdata('info', 'username atau password salah!');
					redirect("auth/", "refresh");
				}
			}

		$this->load->view('login');
	}


	public function logout()
	{
		$this->session->sess_destroy();
		redirect("auth/", "refresh");
	}
}
	