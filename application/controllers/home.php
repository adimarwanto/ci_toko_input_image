<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class home extends CI_Controller
{

	Public function __construct()
	{
		parent::__construct();
		//load model terkait

		//cek user login
		$user_login = $this->session->userdata();
		if (count($user_login) <= 1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		redirect("karyawan/index", "refresh");
	}
	
}
	