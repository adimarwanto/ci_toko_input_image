<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class karyawan extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait (untuk manggil scrip pertama kali di jalankan )
		$this->load->model("karyawan_model");
		$this->load->model("jabatan_model");

		//cek sesi
		$user_login = $this->session->userdata();
		if (count($user_login)<=1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		$this->listkaryawan();
	}
	 public function listkaryawan()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['content']	   = 'forms/list_karyawan';
		
		$this->load->view('home1', $data); 
	}
	public function inputKaryawan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content']	   = 'forms/input_karyawan';

		//if(!empty($_REQUEST)){
			//$m_karyawan = $this->karyawan_model;
			//$m_karyawan->save();
			//redirect("karyawan/index", "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());

		if ($validation->run()) {
			# code...
			$this->karyawan_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("karyawan/index", "refresh");
		}
		$this->load->view('home1',$data); 	
	}
	public function listdetailkaryawan($nik)
	{
				$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
				$data['content']	   = 'forms/detail_k';
		$this->load->view('home1', $data); 	
	}
	public function edit($nik)
	{
				$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
				$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
				$data['content']	   = 'forms/edit_karyawan';
			//if(!empty($_REQUEST)){
			//$m_karyawan = $this->karyawan_model;
			//$m_karyawan->update($nik);
			//redirect("karyawan/index", "refresh");
			//}
		$validation = $this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());

		if ($validation->run()) {
			# code...
			$this->karyawan_model->update($nik);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("karyawan/index", "refresh");
		}
		$this->load->view('home1', $data); 	
	}
	public function delete($nik)
	{
		$m_karyawan = $this->karyawan_model;
		$m_karyawan->delete($nik);
		redirect("karyawan/index", "refresh");
	}
}